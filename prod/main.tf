module "ec2" {
  source          = "../modules/ec2"

  ami             = var.ami
  ec2_type        = var.ec2_type
  key_name        = var.key_name
  ec2_name        = var.ec2_name
  name_ec2_sg     = var.name_ec2_sg
  name_vpc        = var.name_vpc
  depends_on = [
    module.sg
  ]
}

module "network" {
  source         = "../modules/network"

  cidr           = var.cidr
  name_vpc       = var.name_vpc
  az1            = var.az1
  az2            = var.az2
  az3            = var.az3
  subnet1        = var.subnet1
  subnet2        = var.subnet2
  subnet3        = var.subnet3
}

module "sg" {
  source         = "../modules/sg"

  name_ec2_sg    = var.name_ec2_sg
  protocol       = var.protocol
  protocol_all   = var.protocol_all
  name_vpc       = var.name_vpc
  depends_on = [
    module.network
  ]
}

module "alb" {
  source = "../modules/alb"
  
  name_alb    = var.name_alb
  lb_traffic  = var.lb_traffic
  lb_type     = var.lb_type
  name_alb_tg = var.name_alb_tg
  name_vpc    = var.name_vpc
  name_ec2_sg = var.name_ec2_sg
  ec2_name    = var.ec2_name
  depends_on = [
    module.ec2
  ]
}

module "s3" {
  source = "../modules/s3"
  
  name_s3   = var.name_s3
  bucket_acl = var.bucket_acl
}