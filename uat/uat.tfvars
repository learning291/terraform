region = "ap-south-1"
ami = "ami-0300b3c1504b56ca4"
ec2_type = "t2.micro"
key_name = "RM-Windows"
ec2_name = "tf-learn-ec2-uat"
name_ec2_sg = "tf-learn-sg-uat"
protocol = "tcp"
protocol_all = "-1"
name_s3 = "tf-learn-s3-uat"
bucket_acl = "private"
isTrue = "False"
name_alb = "tf-learn-alb-uat"
name_alb_tg = "tf-learn-tg-uat"
vpc_id = "vpc-a39c4bc8"
name_vpc = "tf-learn-vpc-uat"
cidr = "10.17.0.0/16"
subnet1 = "10.17.0.0/20"
subnet2 = "10.17.16.0/20"
subnet3 = "10.17.32.0/20"
az1 = "ap-south-1a"
az2 = "ap-south-1b"
az3 = "ap-south-1c"
name_asg_lb = "tf-learn-asg-alb-uat"
lb_traffic = "false"
name_asg_alb_tg = "tf-learn-asg-tg-uat"
rds_storage = "10"
engine = "mysql"
engine_version = "5.7"
rds_instance = "db.t3.micro"
rds_user = "dbuser"
rds_pwd = "Computer123"
rds_snapshot = "true"
rds_name = "tflearnrdsuat"
lb_type = "application"
env = "uat"