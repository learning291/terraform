variable "lb_type" {}
variable "lb_traffic" {
    type = bool
}
variable "name_alb" {}
variable "name_alb_tg" {}
variable "ec2_name" {}
variable "name_ec2_sg" {}
variable "name_vpc" {}