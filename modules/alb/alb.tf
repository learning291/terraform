data "aws_vpc" "tf-learn-vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.name_vpc}"]
  }
}

data "aws_subnet_ids" "subnet_ids" {
  vpc_id = data.aws_vpc.tf-learn-vpc.id 
}

data "aws_security_group" "tf-learn-sg" {
    filter {
    name   = "tag:Name"
    values = ["${var.name_ec2_sg}"]
  }
}

data "aws_instance" "tf-learn-ec2" {
      filter {
    name   = "tag:Name"
    values = ["${var.ec2_name}"]
  }
}


resource "aws_lb" "tf-learn-alb" {
  name               = var.name_alb
  internal           = var.lb_traffic
  load_balancer_type = var.lb_type
  security_groups    = [data.aws_security_group.tf-learn-sg.id]
  subnets            = data.aws_subnet_ids.subnet_ids.ids
  tags = {
    Name             = var.name_alb
  }
}

resource "aws_lb_target_group" "tf-learn-tg" {
  name              = var.name_alb_tg
  port              = 80
  protocol          = "HTTP"
  vpc_id            = data.aws_vpc.tf-learn-vpc.id

  tags = {
    Name            = var.name_alb_tg
  }
}

resource "aws_lb_listener" "tf-learn-listener" {
  load_balancer_arn = aws_lb.tf-learn-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tf-learn-tg.arn
  }
}

resource "aws_lb_target_group_attachment" "tg_attachment" {
  target_group_arn = aws_lb_target_group.tf-learn-tg.arn
  target_id        = data.aws_instance.tf-learn-ec2.id
  port             = 80
}