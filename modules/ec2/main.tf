data "aws_vpc" "tf-learn-vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.name_vpc}"]
  }
}
data "aws_subnet" "Subnet1" {
  vpc_id = data.aws_vpc.tf-learn-vpc.id
  filter {
    name   = "tag:Name"
    values = ["subnet1"]
  } 
}

data "aws_security_group" "tf-learn-sg" {
    filter {
    name   = "tag:Name"
    values = ["${var.name_ec2_sg}"]
  }
}

resource "aws_instance" "tf-learn-ec2" {
  ami                       = var.ami
  instance_type             = var.ec2_type
  key_name                  = var.key_name
  vpc_security_group_ids    = [data.aws_security_group.tf-learn-sg.id]
  subnet_id                 = data.aws_subnet.Subnet1.id

  tags = {
    Name                    = var.ec2_name
  }
}