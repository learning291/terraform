resource "aws_vpc" "tf-learn-vpc" {
  cidr_block       = var.cidr

  tags = {
    Name = var.name_vpc
  }
}

resource "aws_internet_gateway" "tf-learn-gw" {
  vpc_id = aws_vpc.tf-learn-vpc.id

  tags = {
    Name = "tf-learn-gw"
  }
}

resource "aws_subnet" "subnet1" {
  vpc_id     = aws_vpc.tf-learn-vpc.id
  availability_zone = var.az1
  cidr_block = var.subnet1

  tags = {
    Name = "subnet1"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id     = aws_vpc.tf-learn-vpc.id
  availability_zone = var.az2
  cidr_block = var.subnet2

  tags = {
    Name = "subnet2"
  }
}

resource "aws_subnet" "subnet3" {
  vpc_id     = aws_vpc.tf-learn-vpc.id
  availability_zone = var.az3
  cidr_block = var.subnet3

  tags = {
    Name = "subnet3"
  }
}