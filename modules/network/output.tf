output "vpc_id" {
  value = aws_vpc.tf-learn-vpc.id
}

output "subnet1" {
  value = aws_subnet.subnet1.id
}

output "subnet2" {
  value = aws_subnet.subnet2.id
}

output "subnet3" {
  value = aws_subnet.subnet3.id
}