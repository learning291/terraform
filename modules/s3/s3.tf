resource "aws_s3_bucket" "tf-learn-s3" {
  bucket = var.name_s3
  acl    = var.bucket_acl

  tags = {
    Name = var.name_s3
  }
}