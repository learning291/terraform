data "aws_vpc" "tf-learn-vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.name_vpc}"]
  }
}


resource "aws_security_group" "tf-learn-sg" {
  name              = var.name_ec2_sg
  description       = "Allows communication between alb and ec2"
  vpc_id            = data.aws_vpc.tf-learn-vpc.id
  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = var.protocol
    cidr_blocks     = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = var.protocol
    cidr_blocks     = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = var.protocol_all
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name            = var.name_ec2_sg
  }
}